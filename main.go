package main

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/api"
	"gitlab.com/faw_wal/lightweight-netflix-go/db"
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
)
func setupRouter(server *gin.Engine) {

	//if true {
	//	server.Use(static.Serve("/", static.LocalFile("./averroes-frontend/build", false)))
	//	server.Use(static.Serve("/static", static.LocalFile("./averroes-frontend/build", false)))
	//	server.NoRoute(func(ctx *gin.Context) {
	//		ctx.File("./averroes-frontend/build/index.html")
	//	})
	//} else {
	//	server.StaticFile("/", "websockets/index.html")
	//}
	api.SetupRouter(server)
	//websockets.SetupWSRouter(server)
}

func main()  {
	fmt.Println("test")
	server := gin.Default()
	db.SetupDB()
	setupRouter(server)
	PORT, portErr := os.LookupEnv("PORT")

	if !portErr {
		PORT = "8080"
	}

	//if utils.Contains(
	//	[]string{"development", "staging"},
	//	os.Getenv("ENVIRONMENT"),
	//) {
	//	if debug, exists := os.LookupEnv("DEBUG"); !exists || debug == "false" {
	//		go repeatedactions.Dailyticker()
	//		repeatedactions.SetupCron()
	//	}
	//}

	//runKafkaConsumer()

	server.Run(":" + PORT)
}
