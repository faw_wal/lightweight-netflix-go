package util

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/utils/http"
	"github.com/gin-gonic/gin"
)

func ReturnJsonError(ctx *gin.Context, err error, code int) {
	httpError := http.ToErrorHttp(err)

	if httpError != nil {
		ctx.JSON(code, gin.H{
			"error": httpError.Error(),
			"code":  httpError.Code(),
		})
		return
	}
	ctx.JSON(code, gin.H{
		"error": err.Error(),
	})
}

func ReturnJsonErrorWithData(ctx *gin.Context, err error, data interface{}, code int) {
	ctx.JSON(code, gin.H{
		"error": err.Error(),
		"data":  data,
	})
}

func ReturnJsonSuccessWithMessage(ctx *gin.Context, data interface{}, message string) {
	ctx.JSON(200, gin.H{
		"data":    data,
		"message": message,
	})
}

func ReturnJsonSuccessMessage(ctx *gin.Context, message string) {
	ctx.JSON(200, gin.H{
		"message": message,
	})
}

func ReturnJsonSuccess(ctx *gin.Context, data interface{}) {
	ReturnJsonSuccessWithMessage(ctx, data, "Success")
}

// Note: ctx.Query cache the query after first call
// so it'll be useless to call this function
// after ctx.Query call, in case you need to get query
// before this call, use ctx.BindQuery or ctx.Request.URL.Query()
func AddQuery(ctx *gin.Context, key string, value string) {
	q := ctx.Request.URL.Query()

	q.Add(key, value)

	ctx.Request.URL.RawQuery = q.Encode()
}

// Same note above
func AddQueryArray(ctx *gin.Context, key string, values []string) {
	q := ctx.Request.URL.Query()

	for _, s := range values {
		q.Add(key, s)
	}

	ctx.Request.URL.RawQuery = q.Encode()
}
