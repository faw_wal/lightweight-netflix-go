package api

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/api/movies"
	"github.com/gin-gonic/gin")

// SetupRouter ...
func SetupRouter(gin *gin.Engine) {
	apiGroup := gin.Group("/api")
	movies.SetupRouter(apiGroup)
	//unAuthGroup := gin.Group("/api")

	//unAuthGroup.StaticFile("rand_file", "100MB.bin")

	//webhooks.SetupRouter(apiGroup)

	//onedrive.SetupRouter(unAuthGroup)

	//users.SetupRouter(apiGroup)

	//model.SetupNoAuthRouter(unAuthGroup)

	//apiGroup.Use(middlewares.Auth())
	//{
	//	// resources.SetupRouter(apiGroup)
	//	cameras.SetupRouter(apiGroup)
	//	batchprediction.SetupRouter(apiGroup)
	//	timeseries.SetupRouter(apiGroup)
	//	policies.SetupRouter(apiGroup)
	//	users.SetupUsersRouter(apiGroup)
	//	dataset.SetupRouter(apiGroup)
	//	model.SetupRouter(apiGroup)
	//	files.SetupRouter(apiGroup)
	//	apps.SetupRouter(apiGroup)
	//	executions.SetupRouter(apiGroup)
	//	scheduledExecution.SetupRouter(apiGroup)
	//	payment.SetupRouter(apiGroup)
	//	connector.SetupRouter(apiGroup)
	//	invitation.SetupRouter(apiGroup)
	//	organization.SetupRouter(apiGroup)
	//	stats.SetupRouter(apiGroup)
	//	hoyaApi.SetupRouter(apiGroup)
	//	pdm.SetupPDMRouter(apiGroup)
	//}
}
