package movies

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/api/movies/controllers/v1"
	"github.com/gin-gonic/gin"
)

//SetupRouter ...
func SetupRouter(server *gin.RouterGroup) {
	appsGroup := server.Group("/movies/v1")
	appsGroup.POST("",
		v1.CreateMovie,
	)
	appsGroup.GET("/:id",
		v1.GetMovie,
	)
	appsGroup.GET("",
		v1.GetMovies,
	)
	appsGroup.PUT("/:id",
		v1.UpdateMovie,
	)
	appsGroup.DELETE("/:id",
		v1.DeleteMovie,
	)

}

