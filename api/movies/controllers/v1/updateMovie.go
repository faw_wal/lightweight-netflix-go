package v1

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/injection"
	"gitlab.com/faw_wal/lightweight-netflix-go/usecases/movie"
	"gitlab.com/faw_wal/lightweight-netflix-go/api/util"
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

type updateMovieRequest struct {
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Date        *time.Time `form:"date" time_format:"2006-01-02"`
}

func UpdateMovie(ctx *gin.Context) {

	request := updateMovieRequest{}
	movieId := ctx.Param("id")
	bindError := ctx.ShouldBind(&request)

	if bindError != nil {
		fmt.Println("Bind Error")
		util.ReturnJsonError(ctx, bindError, 400)
		return
	}

	createMovieUseCase := injection.InitUpdateMovie()

	output, executionError := createMovieUseCase.Execute(&movie.UpdateMovieInput{
		MovieId: movieId,
		Name:        request.Name,
		Description: request.Description,
		Date:        request.Date,
	})

	if executionError != nil {
		util.ReturnJsonError(ctx, executionError, 400)
		return
	}

	ctx.JSON(200, gin.H{
		"message": "Movie Updated successfully",
		"data":    output.Movie,
	})
}
