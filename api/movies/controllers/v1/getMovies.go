package v1

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/movies"
	"gitlab.com/faw_wal/lightweight-netflix-go/injection"
	"gitlab.com/faw_wal/lightweight-netflix-go/api/util"
	"errors"
	"github.com/gin-gonic/gin"
)

type GetMoviesRequest struct {
	Page      int64  `form:"page,omitempty"`
	PageLimit int64  `form:"page_limit,omitempty"`
	Name      string `form:"name,omitempty"`
}

func GetMovies(ctx *gin.Context) {

	request := GetMoviesRequest{}

	ctx.ShouldBindQuery(&request)

	repo := injection.InitMovieRepo()

	movies := repo.GetList(movie.GetListFilters{
		Page:      &request.Page,
		PageLimit: &request.PageLimit,
		Name:      &request.Name,
	})

	if movies == nil {
		util.ReturnJsonError(ctx, errors.New("Error"), 400)
		return
	}

	ctx.JSON(200, gin.H{
		"data": *movies,
	})

}
