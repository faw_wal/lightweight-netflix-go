package v1

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/injection"
	"gitlab.com/faw_wal/lightweight-netflix-go/api/util"
	"errors"
	"github.com/gin-gonic/gin"
)

func DeleteMovie(ctx *gin.Context) {
	id := ctx.Param("id")
	movieRepo := injection.InitMovieRepo()
	movie := movieRepo.FindById(id)
	if movie == nil {
		util.ReturnJsonError(ctx, errors.New("Movie not Found"), 400)
		return
	}
	err := movieRepo.Delete(id)

	if err != nil {
		util.ReturnJsonError(ctx, err, 400)
		return
	}
	util.ReturnJsonSuccess(ctx, gin.H{
		"message": "Movie was deleted successfully",
	})
	return
}
