package v1

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/injection"
	"gitlab.com/faw_wal/lightweight-netflix-go/api/util"
	"errors"
	"github.com/gin-gonic/gin"
)


func GetMovie(ctx *gin.Context) {
	id := ctx.Param("id")


	repo := injection.InitMovieRepo()

	movie := repo.FindById(id)

	if movie == nil {
		util.ReturnJsonError(ctx, errors.New("Movie Not Found"), 404)
		return
	}

	ctx.JSON(200, gin.H{
		"data": *movie,
	})

}

