package injection

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db"
	movies2 "gitlab.com/faw_wal/lightweight-netflix-go/db/movies"
	"gitlab.com/faw_wal/lightweight-netflix-go/usecases/movie"
)
func InitCreateNewMovie() *movie.CreateMovie {
	database := db.NewDB()
	iMovieRepo := movies2.NewMovieRepo(database)
	createMovie := movie.NewCreateMovie(iMovieRepo)
	return createMovie
}
func InitUpdateMovie() *movie.UpdateMovie {
	database := db.NewDB()
	iMovieRepo := movies2.NewMovieRepo(database)
	updateMovie := movie.NewUpdateMovie(iMovieRepo)
	return updateMovie
}
func InitMovieRepo() movies2.IMovieRepo {
	database := db.NewDB()
	iMovieRepo := movies2.NewMovieRepo(database)
	return iMovieRepo
}