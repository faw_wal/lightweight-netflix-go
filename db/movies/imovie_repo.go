package movie

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/pagination"
)
type IMovieRepo interface {
	Save(movie Movie) (*Movie, error)

	GetMovie(filter interface{}) *Movie

	GetList(filters GetListFilters) *pagination.Page

	FindByName(name string) *Movie

	FindById(id string) *Movie

	Delete(movieId string) error

	Update(movie *Movie) (*Movie, error)
}

type GetListFilters struct {
	Name    *string
	Page      *int64
	PageLimit *int64
}
