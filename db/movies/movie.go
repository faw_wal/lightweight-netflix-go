package movie

import (
	"time"
)

type 	Movie struct {
	ID          string    `bson:"_id" json:"id"`
	Name        string    `bson:"name" json:"name"`
	Description string    `bson:"description" json:"description"`
	Date       *time.Time `bson:"date" json:"date"`
	CoverUrl    string    `bson:"cover_url" json:"cover_url"`
	Rate        int16     `bson:"rate" json:"rate"`
	CreatedAt   time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt   time.Time `bson:"updated_at" json:"updated_at"`
}
