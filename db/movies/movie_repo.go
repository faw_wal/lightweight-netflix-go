package movie

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db"
	"gitlab.com/faw_wal/lightweight-netflix-go/db/filter"
	"gitlab.com/faw_wal/lightweight-netflix-go/db/pagination"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type MovieRepo struct {
	dbClient   *mongo.Database
	collection *mongo.Collection
}

func NewMovieRepo(dbClient *mongo.Database) IMovieRepo {
	return &MovieRepo{
		dbClient:   dbClient,
		collection: dbClient.Collection(db.MoviesCollection),
	}
}

func (movieRepo *MovieRepo) Save(movie Movie) (*Movie, error) {
	movie.CreatedAt = time.Now()
	movie.UpdatedAt = time.Now()
	_, err := movieRepo.collection.InsertOne(context.TODO(), movie)

	if err != nil {
		return nil, err
	}

	return &movie, err
}
func (repo *MovieRepo) GetMovie(filter interface{}) *Movie {

	movie := Movie{}

	err := repo.collection.FindOne(context.TODO(), filter).Decode(&movie)

	if err != nil {
		return nil
	}

	return &movie
}

func (repo *MovieRepo) GetList(filters GetListFilters) *pagination.Page {

	filter := filter.FilterPool()
	//filter.Match("name", filters.Name),

	page := pagination.NewPage(filters.Page, filters.PageLimit)

	options := db.GetPaginationOptions(*page)

	cursor, err := repo.collection.Find(context.TODO(), filter, options)

	if err != nil {
		fmt.Println("ERROR", err)
		return nil
	}

	movies := []Movie{}

	err = cursor.All(context.TODO(), &movies)

	if err != nil {
		return nil
	}

	cursor.Close(context.TODO())

	// Count the documents to get the page total
	count, _ := repo.collection.CountDocuments(context.TODO(), filter)

	page.Data = movies
	page.SetTotalPages(count)

	return page
}

func (repo *MovieRepo) FindByName(name string) *Movie {
	filter := bson.D{
		primitive.E{Key: "name", Value: name},
	}
	return repo.GetMovie(filter)
}

func (repo *MovieRepo) FindById(id string) *Movie {
	filter := bson.D{
		primitive.E{Key: "_id", Value: id},
	}
	return repo.GetMovie(filter)
}

func (repo *MovieRepo) Delete(movieId string) error {
	filter := bson.D{
		primitive.E{Key: "_id", Value: movieId},
	}
	_, err := repo.collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		return err
	}

	return nil
}

func (repo *MovieRepo) Update(movie *Movie) (*Movie, error) {
	filter := bson.D{
		primitive.E{Key: "_id", Value: movie.ID},
	}
	movie.UpdatedAt = time.Now()
	_, err := repo.collection.ReplaceOne(context.TODO(), filter, movie)

	if err != nil {
		return nil, err
	}

	return movie, nil
}
