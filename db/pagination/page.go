package pagination

import (
	"math"
)

const (
	DefaultPageSize   = int64(10)
	DefaultPageNumber = int64(1)
)

type Page struct {
	TotalPages      int64       `json:"total_pages"`
	CurrentPage     int64       `json:"current_page"`
	PageSize        int64       `json:"page_limit"`
	AllRecordsCount int64       `json:"records_count"`
	Data            interface{} `json:"data"`
}

func (p *Page) SetAllRecordsCount(recordsCount int64) {
	p.SetTotalPages(recordsCount)
}

func (p *Page) SetTotalPages(recordsCount int64) {
	p.AllRecordsCount = recordsCount

	total := float64(p.AllRecordsCount) / float64(p.PageSize)

	p.TotalPages = int64(math.Ceil(total))
}

func NewPage(page *int64, limit *int64) *Page {
	currentPage := DefaultPageNumber
	pageSize := DefaultPageSize

	if page != nil {
		currentPage = int64(math.Max(float64(*page), float64(1)))
	}

	if limit != nil {
		if *limit == 0 {
			pageSize = DefaultPageSize
		} else {
			pageSize = int64(*limit)
		}
	}

	return &Page{
		CurrentPage: currentPage,
		PageSize:    pageSize,
	}
}

func DefaultPage() *Page {
	return &Page{
		CurrentPage: DefaultPageNumber,
		PageSize:    DefaultPageSize,
	}
}
