package db

import (
	"context"
	"fmt"
	"gitlab.com/faw_wal/lightweight-netflix-go/db/filter"
	"gitlab.com/faw_wal/lightweight-netflix-go/db/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//WatchCollectionOptions ..
type WatchCollectionOptions struct {
	FullDocument bool
}

func GetPaginationOptions(page pagination.Page) *options.FindOptions {
	options := options.FindOptions{}

	options.SetSkip((page.CurrentPage - 1) * page.PageSize)
	options.SetLimit(page.PageSize)

	return &options
}

func ReadCountFromCursor(cursor mongo.Cursor) int64 {
	count := int64(0)

	for cursor.Next(context.TODO()) {
		data := bson.M{}
		cursor.Decode(&data)
		fmt.Println("data", data)
		count = int64(data["count"].(int32))
	}

	return count
}

func ConvertFiltersToPipeline(filters interface{}) mongo.Pipeline {
	pipeline := mongo.Pipeline{}
	pipeline = append(pipeline, bson.D{primitive.E{Key: "$match", Value: filters}})

	return pipeline
}

func GetPaginationPipelines(page pagination.Page) mongo.Pipeline {
	return []bson.D{
		bson.D{primitive.E{Key: "$skip", Value: (page.CurrentPage - 1) * page.PageSize}},
		bson.D{primitive.E{Key: "$limit", Value: page.PageSize}},
	}
}

func GetCountPipelineOf(pipeline mongo.Pipeline) mongo.Pipeline {
	countPipeline := make(mongo.Pipeline, len(pipeline))

	copy(countPipeline, pipeline)

	countPipeline = append(countPipeline, bson.D{{"$count", "count"}})

	return countPipeline
}

// GetSortPipeline
// sortingMap, ex: {"created_at": 1} or {"created_at": -1}, 1: for asc, -1: desc
func GetSortPipeline(sortingMap filter.SortFilter) mongo.Pipeline {
	pipeline := mongo.Pipeline{}
	pipeline = append(pipeline, bson.D{primitive.E{Key: "$sort", Value: sortingMap}})

	return pipeline
}

func GetProjectionPipeline(project map[string]int) mongo.Pipeline {
	return []bson.D{
		bson.D{primitive.E{Key: "$project", Value: project}},
	}
}
