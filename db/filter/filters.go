package filter

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"reflect"
)

func FilterPool(filters ...*primitive.E) *bson.D {

	pool := bson.D{}

	for _, filter := range filters {
		if filter != nil {
			pool = append(pool, *filter)
		}
	}

	return &pool
}

func isNull(value interface{}) bool {
	reflectedValue := reflect.ValueOf(value)
	return reflectedValue.Kind() == reflect.Ptr && reflectedValue.IsNil()
}

func getNonNullExpressions(expressions ...*primitive.E) []bson.D {
	nonNullExpressions := []bson.D{}

	for _, e := range expressions {
		if e != nil {
			nonNullExpressions = append(nonNullExpressions, bson.D{*e})
		}
	}

	return nonNullExpressions
}

func Match(key string, value interface{}) *primitive.E {
	if isNull(value) || value == "" || key == "" {
		return nil
	}
	return &primitive.E{
		Key:   key,
		Value: value,
	}
}

func NotEqual(key string, value interface{}) *primitive.E {
	if isNull(value) || value == "" || key == "" {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$ne", Value: value},
		},
	}
}

func LessThan(key string, value interface{}) *primitive.E {
	if isNull(value) || value == "" || key == "" {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$lt", Value: value},
		},
	}
}

func LessThanOrEqual(key string, value interface{}) *primitive.E {
	if isNull(value) || value == "" || key == "" {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$lte", Value: value},
		},
	}
}

func GreaterThan(key string, value interface{}) *primitive.E {
	if isNull(value) || value == "" || key == "" {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$gt", Value: value},
		},
	}
}

func GreaterThanOrEqual(key string, value interface{}) *primitive.E {
	if isNull(value) || value == "" || key == "" {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$gte", Value: value},
		},
	}
}

func In(key string, value interface{}) *primitive.E {
	if isNull(value) || key == "" {
		return nil
	}

	// Check if the value is an array so it mustn't be an empty
	reflectedValue := reflect.ValueOf(value)
	if reflectedValue.Kind() == reflect.Slice && reflectedValue.Len() == 0 {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$in", Value: value},
		},
	}
}

func Regex(key string, value string) *primitive.E {
	if key == "" || value == "" {
		return nil
	}

	return &primitive.E{
		Key: key,
		Value: primitive.D{
			primitive.E{Key: "$regex", Value: value}, primitive.E{Key: "$options", Value: "i"},
		},
	}
}

func Or(expressions ...*primitive.E) *primitive.E {
	orExpressions := getNonNullExpressions(expressions...)

	if len(orExpressions) == 0 {
		return nil
	}

	return &primitive.E{
		Key:   "$or",
		Value: orExpressions,
	}
}

func And(expressions ...*primitive.E) *primitive.E {
	andExpressions := getNonNullExpressions(expressions...)

	if len(andExpressions) == 0 {
		return nil
	}

	return &primitive.E{
		Key:   "$and",
		Value: andExpressions,
	}
}
