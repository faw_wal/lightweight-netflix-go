package filter

import (
	"strings"
)

type SortOrderType int

type SortFilter map[string]SortOrderType

const (
	ASC  SortOrderType = 1
	DESC SortOrderType = -1
)

type Sort struct {
	Key       string
	OrderType SortOrderType
}

// convert "sortKet" -> {Key: "sortKey", OrderType: ASC}
// convert "-sortKet" -> {Key: "sortKey", OrderType: DESC}
func parseSort(sort string) *Sort {
	if sort == "" {
		return nil
	}

	if strings.HasPrefix(sort, "-") {
		return &Sort{
			Key:       string(sort[1:]), // Get substring of `sort`, ex: `-key` -> `key`
			OrderType: DESC,
		}
	} else {
		return &Sort{
			Key:       sort,
			OrderType: ASC,
		}
	}

}

// Praseing SortFilter map from a strings
// ex: [-name, id] -> {name: DESC, id:ASC}
func PraseSortFilter(sorts ...string) SortFilter {
	filter := SortFilter{}

	for _, sortString := range sorts {

		sort := parseSort(sortString)

		if sort != nil {
			filter[sort.Key] = sort.OrderType
		}
	}

	return filter
}
