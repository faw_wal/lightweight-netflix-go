package db

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//DB ..
var DB *mongo.Database

func NewDB() *mongo.Database {
	return DB
}

//SetupDB setup database
func SetupDB() error {
	fmt.Println(".................... connecting to DB", time.Now())
	mongodCloud, exists := os.LookupEnv("MONGO_CLOUD")
	connectionString := "mongodb://localhost:27017"
	fmt.Println(exists && mongodCloud == "true")
	//if exists && mongodCloud == "true" {
	//	dbUser := secrets.DBUser
	//	dbPass := secrets.DBPass
	//	dbSeeds := secrets.DBSeeds
	//	connectionString = "mongodb+srv://" + dbUser + ":" + dbPass + "@" + dbSeeds
	//
	//}
	client, err := mongo.NewClient(options.Client().ApplyURI(connectionString))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	dbName := "netflix"
	fmt.Println("Connected to mongo server")
	DB = client.Database(dbName)
	fmt.Println("database selected")
	return nil
}
