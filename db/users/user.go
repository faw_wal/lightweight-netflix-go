package user

import (
	"time"
)

type User struct {
	ID        string    `bson:"_id" json:"id"`
	FullName  string    `bson:"full_name" json:"full_name"`
	Age       int8      `bson:"age" json:"age"`
	Email     string    `bson:"date" json:"date"`
	Roles     []string  `json:"roles,omitempty" bson:"roles"`
	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
}
