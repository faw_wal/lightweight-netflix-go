package user

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db"
	"gitlab.com/faw_wal/lightweight-netflix-go/db/filter"
	"gitlab.com/faw_wal/lightweight-netflix-go/db/pagination"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type UserRepo struct {
	dbClient   *mongo.Database
	collection *mongo.Collection
}

func NewUserRepo(dbClient *mongo.Database) IUserRepo {
	return &UserRepo{
		dbClient:   dbClient,
		collection: dbClient.Collection(db.UsersCollection),
	}
}

func (userRepo *UserRepo) Save(user User) (*User, error) {
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()
	_, err := userRepo.collection.InsertOne(context.TODO(), user)

	if err != nil {
		return nil, err
	}

	return &user, err
}
func (repo *UserRepo) GetUser(filter interface{}) *User {

	user := User{}

	err := repo.collection.FindOne(context.TODO(), filter).Decode(&user)

	if err != nil {
		return nil
	}

	return &user
}

func (repo *UserRepo) GetList(filters GetListFilters) *pagination.Page {

	filter := filter.FilterPool()
	//filter.Match("name", filters.Name),

	page := pagination.NewPage(filters.Page, filters.PageLimit)

	options := db.GetPaginationOptions(*page)

	cursor, err := repo.collection.Find(context.TODO(), filter, options)

	if err != nil {
		fmt.Println("ERROR", err)
		return nil
	}

	users := []User{}

	err = cursor.All(context.TODO(), &users)

	if err != nil {
		return nil
	}

	cursor.Close(context.TODO())

	// Count the documents to get the page total
	count, _ := repo.collection.CountDocuments(context.TODO(), filter)

	page.Data = users
	page.SetTotalPages(count)

	return page
}

func (repo *UserRepo) FindById(id string) *User {
	filter := bson.D{
		primitive.E{Key: "_id", Value: id},
	}
	return repo.GetUser(filter)
}
func (repo *UserRepo) FindByEmail(email string) *User {
	filter := bson.D{
		primitive.E{Key: "email", Value: email},
	}
	return repo.GetUser(filter)
}

func (repo *UserRepo) Delete(userId string) error {
	filter := bson.D{
		primitive.E{Key: "_id", Value: userId},
	}
	_, err := repo.collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		return err
	}

	return nil
}

func (repo *UserRepo) Update(user *User) (*User, error) {
	filter := bson.D{
		primitive.E{Key: "_id", Value: user.ID},
	}
	user.UpdatedAt = time.Now()
	_, err := repo.collection.ReplaceOne(context.TODO(), filter, user)

	if err != nil {
		return nil, err
	}

	return user, nil
}
