package user

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/pagination"
)

type IUserRepo interface {
	Save(user User) (*User, error)

	GetUser(filter interface{}) *User

	GetList(filters GetListFilters) *pagination.Page

	FindById(id string) *User

	FindByEmail(email string) *User

	Delete(userId string) error

	Update(user *User) (*User, error)
}

type GetListFilters struct {
	FullName  *string
	Page      *int64
	PageLimit *int64
}
