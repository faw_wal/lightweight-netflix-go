package http

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/utils/http/client"
)

func NewLabelingHttpClient() client.IHttpClient {
	baseUrl := ""

	return client.NewHttpClient(&baseUrl)
}
