package http

import (
	"reflect"
)

type ErrorHttp struct {
	HttpCode int
	Message  string
}

func (e *ErrorHttp) Error() string {
	return e.Message
}

func (e *ErrorHttp) Code() int {
	return e.HttpCode
}

func NewHttpError(code int, message string) error {
	return &ErrorHttp{code, message}
}

func IsErrorHttp(err error) bool {
	_, hasCode := reflect.TypeOf(err).MethodByName("Code")
	return hasCode
}

func ToErrorHttp(err error) *ErrorHttp {
	if IsErrorHttp(err) {
		return &ErrorHttp{
			HttpCode: int(reflect.ValueOf(err).MethodByName("Code").Call([]reflect.Value{})[0].Int()),
			Message:  err.Error(),
		}
	}
	return nil
}
