package client

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	httpUrl "net/url"
	"strconv"
	"strings"

	//"github.com/gojek/heimdall/v7/hystrix"
)

type HttpClient struct {
	BaseUrl *string
	//client  *hystrix.Client
}

func NewHttpClient(baseUrl *string) IHttpClient {
	client := HttpClient{
		BaseUrl: baseUrl,
		//client:  hystrix.NewClient(),
	}

	return &client
}

//func (c *HttpClient) Post(url string, body interface{}, config Config) (*HttpResponse, error) {
//	url = c.mergeWithBaseUrl(url)
//
//	response, err := c.client.Post(url, c.marshalBody(body), *c.parseHeaders(config))
//
//	if err != nil {
//		return nil, err
//	}
//
//	return &HttpResponse{
//		StatusCode: response.StatusCode,
//		Decode:     c.decodeFunc(response.Body),
//	}, nil
//}

func (c *HttpClient) PostForm(url string, form Form, config Config) (*HttpResponse, error) {
	url = c.mergeWithBaseUrl(url)

	data := c.convertFormToUrlValues(form)

	client := &http.Client{}

	request, err := http.NewRequest("POST", url, strings.NewReader(data.Encode()))

	if err != nil {
		return nil, err
	}

	if config.hasHeaders() {
		request.Header = *config.Headers
	}

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	response, err := client.Do(request)

	if err != nil {
		return nil, err
	}

	return &HttpResponse{
		StatusCode: response.StatusCode,
		Decode:     c.decodeFunc(response.Body),
	}, nil
}

func (c *HttpClient) marshalBody(body interface{}) io.Reader {
	bodyBytes, _ := json.Marshal(body)

	return bytes.NewReader(bodyBytes)
}

func (c *HttpClient) decodeFunc(body io.ReadCloser) DecodeFunc {
	return func(target interface{}) error {
		defer body.Close()

		return json.NewDecoder(body).Decode(target)
	}
}

func (c *HttpClient) mergeWithBaseUrl(url string) string {
	if c.BaseUrl == nil || strings.HasPrefix(url, "http") || strings.HasPrefix(url, "https") {
		return url
	}

	if strings.HasSuffix(*c.BaseUrl, "/") || strings.HasPrefix(url, "/") {
		return *c.BaseUrl + url
	}
	return *c.BaseUrl + "/" + url
}

func (c *HttpClient) parseHeaders(config Config) *http.Header {
	headers := &http.Header{}

	if config.hasHeaders() {
		headers = config.Headers
	}

	c.addDefaultHeaders(headers)
	return headers
}

func (c *HttpClient) addDefaultHeaders(headers *http.Header) {
	headers.Add("Content-Type", "application/json")
}

func (c *HttpClient) convertFormToUrlValues(m Form) httpUrl.Values {
	values := httpUrl.Values{}

	for key, value := range m {
		values.Set(key, value)
	}

	return values
}
