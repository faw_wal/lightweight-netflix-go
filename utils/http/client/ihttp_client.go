package client

import "net/http"

type IHttpClient interface {
	//Post(url string, body interface{}, config Config) (*HttpResponse, error)

	PostForm(url string, form Form, config Config) (*HttpResponse, error)
}

type HttpResponse struct {
	StatusCode int
	Decode     DecodeFunc
}

type Config struct {
	Headers *http.Header
}

func (c *Config) hasHeaders() bool {
	return c.Headers != nil
}

type DecodeFunc func(target interface{}) error

type Form map[string]string
