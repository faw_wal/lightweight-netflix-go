package user

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/users"
	"time"

	//appConstants "bitbucket.org/averroes_ai/averroes-api/db/app/constants"
	//dataConstants "bitbucket.org/averroes_ai/averroes-api/db/dataset/constants"
)

type userValidator struct {
	userRepo user.IUserRepo
}

type validationParams struct {
	name        string
	description string
	date        *time.Time
}

func (v *userValidator) isValidUser(params *validationParams) error {
	return nil
}
