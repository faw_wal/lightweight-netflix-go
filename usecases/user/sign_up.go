package user

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/users"
)

type SignUpUser struct {
	userRepo user.IUserRepo
}

type SignUpUserInput struct {
	Email    string
	Age      int8
	FullName string
	Password string
}

func NewSignUpUser(userRepo user.IUserRepo) *SignUpUser {
	return &SignUpUser{
		userRepo: userRepo,
	}
}

func (c *SignUpUser) execute(input SignUpUserInput) *user.User {
	return nil
}
