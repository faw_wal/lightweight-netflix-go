package movie

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/movies"
	"time"

	//appConstants "bitbucket.org/averroes_ai/averroes-api/db/app/constants"
	//dataConstants "bitbucket.org/averroes_ai/averroes-api/db/dataset/constants"
)

type movieValidator struct {
	movieRepo movie.IMovieRepo
}

type validationParams struct {
	name        string
	description string
	date        *time.Time
}

func (v *movieValidator) isValidApp(params *validationParams) error {
	return nil
}
