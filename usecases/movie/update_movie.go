package movie

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/movies"
	"errors"
	"time"
)

type UpdateMovie struct {
	movieRepo movie.IMovieRepo
	validator movieValidator
}

type UpdateMovieInput struct {
	MovieId     string
	Name        string
	Description string
	Date        *time.Time
}

type UpdateMovieOutput struct {
	Movie movie.Movie
}

func NewUpdateMovie(repo movie.IMovieRepo) *UpdateMovie {
	return &UpdateMovie{
		movieRepo: repo,
		validator: movieValidator{
			movieRepo: repo,
		},
	}
}
func (c *UpdateMovie) Execute(input *UpdateMovieInput) (*UpdateMovieOutput, error) {

	movie := c.movieRepo.FindById(input.MovieId)

	if movie == nil {
		return nil, errors.New("Movie Not Found")
	}
	validationParams := getValidationParams(*movie, *input)
	validationError := c.validator.isValidApp(validationParams)
	if validationError != nil {
		return nil, validationError
	}
	movie.Name = input.Name
	movie.Description = input.Description
	movie.Date = input.Date
	// Update the Movie
	savedMovie, err := c.movieRepo.Update(movie)

	if err != nil {
		return nil, err
	}

	return &UpdateMovieOutput{
		Movie: *savedMovie,
	}, nil
}

func getValidationParams(movie movie.Movie, input UpdateMovieInput) *validationParams {

	validatorParams := validationParams{}

	if movie.Name != input.Name {
		validatorParams.name = input.Name
	}
	if movie.Description != input.Description {
		validatorParams.description = input.Description
	}
	if movie.Date != input.Date {
		validatorParams.date = input.Date
	}
	return &validatorParams
}
