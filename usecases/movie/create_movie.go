package movie

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/movies"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type CreateMovie struct {
	movieRepo movie.IMovieRepo
	validator movieValidator
}

type CreateMovieInput struct {
	Name        string
	Description string
	Date        *time.Time
}

type CreateMovieOutput struct {
	Movie movie.Movie
}

func NewCreateMovie(repo movie.IMovieRepo) *CreateMovie {
	return &CreateMovie{
		movieRepo: repo,
		validator: movieValidator{
			movieRepo: repo,
		},
	}
}
func (c *CreateMovie) Execute(input *CreateMovieInput) (*CreateMovieOutput, error) {

	validationError := c.validator.isValidApp(&validationParams{
		name:        input.Name,
		description: input.Description,
		date:        input.Date,
	})
	if validationError != nil {
		return nil, validationError
	}
	movieId := primitive.NewObjectID().Hex()
	// Create app instance
	movieToSave := movie.Movie{
		ID:          movieId,
		Name:        input.Name,
		Description: input.Description,
		Date:        input.Date,
	}

	// Save the app
	savedMovie, err := c.movieRepo.Save(movieToSave)

	if err != nil {
		return nil, err
	}

	return &CreateMovieOutput{
		Movie: *savedMovie,
	}, nil
}
