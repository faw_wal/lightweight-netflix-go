package movie

import (
	"gitlab.com/faw_wal/lightweight-netflix-go/db/movies"
	"errors"
)

type DeleteMovie struct {
	movieRepo movie.IMovieRepo
	validator movieValidator
}

func NewUpdateApp(repo movie.IMovieRepo) *DeleteMovie {
	return &DeleteMovie{
		movieRepo: repo,
	}
}

func (u *DeleteMovie) Execute(movieId string) error {
	movie := u.movieRepo.FindById(movieId)
	if movie == nil {
		return errors.New("Movie Not Found")
	}
	error := u.movieRepo.Delete(movieId)
	if error != nil {
		return error
	}

	return nil
}
